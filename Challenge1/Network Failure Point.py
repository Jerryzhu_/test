#!/user/bin/env python
# -*- coding:utf-8 -*-
# Author: Zhongqi Zhu
# Create：2020-05-18


class Router:
    def __init__(self, label):
        self.label = label
        self.inbound_links = []
        self.outbound_links = []

    def set_inbound_link(self, label):
        self.inbound_links.append(label)

    def set_outbound_link(self, label):
        self.outbound_links.append(label)

    def get_inbound_links(self):
        return len(self.inbound_links)

    def get_outbound_links(self):
        return len(self.outbound_links)

    def get_total_links(self):
        return self.get_inbound_links() + self.get_outbound_links()


def identify_router(pool, graph):
    if len(graph) < 2 or ' -> ' not in graph:
        return []
    result_pool = []
    max_links = 0
    input_graph = list(map(int, graph.split(' -> ')))
    pre_router_index = input_graph[0]

    for node in input_graph[1:]:  # record the inbound link and outbound link for each router
        pool[pre_router_index-1].set_outbound_link(node)
        pool[node-1].set_inbound_link(pre_router_index)
        pre_router_index = node

    for router in pool:  # compare each router and come up with the maximum inbound and outbound link router
        temp = router.get_total_links()
        if temp > max_links:
            max_links = temp
            result_pool = [str(router.label)]
        elif temp == max_links:
            result_pool += [str(router.label)]
    return result_pool


if __name__ == '__main__':
    router_pool = []
    for num in range(1, 7):
        router_pool.append(Router(num))
    result1 = identify_router(router_pool, graph='1 -> 2 -> 3 -> 5 -> 2 -> 1')
    print('The node with the most number of connections: ' + ','.join(result1))

    router_pool = []
    for num in range(1, 7):
        router_pool.append(Router(num))
    result2 = identify_router(router_pool, graph='1 -> 3 -> 5 -> 6 -> 4 -> 5 -> 2 -> 6')
    print('The node with the most number of connections: ' + ','.join(result2))

    router_pool = []
    for num in range(1, 7):
        router_pool.append(Router(num))

    result3 = identify_router(router_pool, graph='2 -> 4 -> 6 -> 2 -> 5 -> 6')
    print('The node with the most number of connections: ' + ','.join(result3))

# time complexity O(n) for iterating the input graph
