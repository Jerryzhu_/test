from django_registration.forms import RegistrationForm
from django import forms
from signup.models import UserProfile


class Subscription(RegistrationForm):

    class Meta(RegistrationForm.Meta):
        model = UserProfile
        fields = [
            "username",
            "email",
            "password1",
            "password2",
        ]
