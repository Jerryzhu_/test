from django.urls import path, include
from django_registration.backends.one_step.views import RegistrationView
from django.contrib.auth import views as auth_views
from signup.forms import Subscription
from . import views

urlpatterns = [
    path('login/', views.log_in, name='log_in'),
    path('', views.home, name='home'),
    path('accounts/login/', auth_views.LoginView.as_view(template_name='login.html'),
         name='django_login'),
    path('accounts/login/success/', views.free_trial, name='success_login'),
    path('accounts/logout/',
         auth_views.LogoutView.as_view(next_page='home'), name='django_logout'),
    path('accounts/register/', RegistrationView.as_view(form_class=Subscription, success_url='/accounts/login/success/'),
         name='django_registration_register'),
    path('accounts/', include('django_registration.backends.one_step.urls')),
    path('free_trial/', views.free_trial, name='trial'),
    path('accounts/', include('django.contrib.auth.urls')),

    # path('success/', views.success, name='success'),
]
