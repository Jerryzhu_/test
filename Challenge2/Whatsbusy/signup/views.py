from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login
from signup.forms import Subscription
from django.contrib.auth.models import User
from django.contrib.auth.forms import AuthenticationForm
from django.conf import settings
from django.http import HttpResponse
import stripe


def home(request):
    return render(request, 'home.html')


def log_in(request):
    if request.method == "POST":
        form = AuthenticationForm(request=request, data=request.POST)
        if form.is_valid():
            username = request.POST['username']
            password = request.POST['password']
            user = authenticate(username=username, password=password)
            if user:
                login(request, user)
                return render(request, 'home.html', {'form': form})
    else:
        form = AuthenticationForm()
    return render(request, 'login.html', {'form': form})


def free_trial(request):
    stripe.api_key = settings.STRIPE_SECRET_KEY
    product = stripe.Product.create(
        name='Blue banana',
    )
    intent = stripe.PaymentIntent.create(
        amount=4999,
        currency='usd',
        # Verify your integration in this guide by including this parameter
        metadata={'integration_check': 'accept_a_payment'},
    )
    price = stripe.Price.create(
        unit_amount=2000,
        currency="usd",
        recurring={"interval": "month"},
        product=product["id"],
    )
    session = stripe.checkout.Session.create(
        payment_method_types=['card'],
        line_items=[{
            'price': price["id"],
            'quantity': 1,
        }],
        mode='subscription',
        success_url='https://example.com/success?session_id={CHECKOUT_SESSION_ID}',
        cancel_url='https://example.com/cancel',
    )
    print(session)
    return render(request, 'home.html')
