from django.db import models
from django.utils import timezone
# Create your models here.
from django.contrib.auth.models import AbstractUser


class UserProfile(AbstractUser):
    date = models.DateTimeField("date joined", default=timezone.now)
