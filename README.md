I am still dealing with the interaction with the stripe API. The current progress is that I can submit the application to the stripe website. The system can already sign up using django registration. The user information is stored in the MySQL database.

I can understand what needs to be resolved in the future: users who provide credit card information when they register will automatically have a 7-day free Premium service, and future payment will be set so that the payment will be automatically made in seven days. Users have three stages which are：
	(1) registered for more than 7 days and are paying
	(2) registered for more than 7 days and are not paying
	(1) registered for less than 7 days
